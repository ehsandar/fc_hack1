def assignment_1() -> list:
    return [1, 2, 3, 4]


def assignment_2() -> str:
    return str([12, 13, 14, 15])


assignments = [assignment_1, assignment_2]


def get_solutions():
    solutions = []

    for sol in assignments:
        if sol.__annotations__['return'] is list:
            ans_type = 'answer_list'
        else:
            ans_type = 'answer_mono'

        solutions.append({
            'title': sol.__name__,
            'slug': sol.__name__,
            ans_type: sol(),
        })

    return solutions
