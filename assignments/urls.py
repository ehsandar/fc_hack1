from django.urls import path, include

from assignments.views import assignments

urlpatterns = [
    path('assignments/', assignments),
]
