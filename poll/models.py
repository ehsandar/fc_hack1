from django.contrib.auth.models import User
from django.db import models


class Poll(models.Model):
    title = models.CharField(max_length=64)
    expire_date = models.DateTimeField(null=True)


class Choice(models.Model):
    poll = models.ForeignKey('Poll', models.CASCADE)
    title = models.CharField(max_length=64)


class Vote(models.Model):
    creator = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    choice = models.ForeignKey('Choice', models.CASCADE)
