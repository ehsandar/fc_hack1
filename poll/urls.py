from django.urls import path, include

from poll.views import PollView

urlpatterns = [
    path('poll/<int:poll_id>/', PollView.as_view()),
]
